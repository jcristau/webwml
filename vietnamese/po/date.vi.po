#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml date\n"
"PO-Revision-Date: 2015-07-03 09:03+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#. List of weekday names (used in modification dates)
#: ../../english/template/debian/ctime.wml:11
msgid "Sun"
msgstr "CN"

#: ../../english/template/debian/ctime.wml:12
msgid "Mon"
msgstr "T2"

#: ../../english/template/debian/ctime.wml:13
msgid "Tue"
msgstr "T3"

#: ../../english/template/debian/ctime.wml:14
msgid "Wed"
msgstr "T4"

#: ../../english/template/debian/ctime.wml:15
msgid "Thu"
msgstr "T5"

#: ../../english/template/debian/ctime.wml:16
msgid "Fri"
msgstr "T6"

#: ../../english/template/debian/ctime.wml:17
msgid "Sat"
msgstr "T7"

#. List of month names (used in modification dates, and may be used in news 
#. listings)
#: ../../english/template/debian/ctime.wml:23
msgid "Jan"
msgstr "Th1"

#: ../../english/template/debian/ctime.wml:24
msgid "Feb"
msgstr "Th2"

#: ../../english/template/debian/ctime.wml:25
msgid "Mar"
msgstr "Th3"

#: ../../english/template/debian/ctime.wml:26
msgid "Apr"
msgstr "Th4"

#: ../../english/template/debian/ctime.wml:27
msgid "May"
msgstr "Th5"

#: ../../english/template/debian/ctime.wml:28
msgid "Jun"
msgstr "Th6"

#: ../../english/template/debian/ctime.wml:29
msgid "Jul"
msgstr "Th7"

#: ../../english/template/debian/ctime.wml:30
msgid "Aug"
msgstr "Th8"

#: ../../english/template/debian/ctime.wml:31
msgid "Sep"
msgstr "Th9"

#: ../../english/template/debian/ctime.wml:32
msgid "Oct"
msgstr "Th10"

#: ../../english/template/debian/ctime.wml:33
msgid "Nov"
msgstr "Th11"

#: ../../english/template/debian/ctime.wml:34
msgid "Dec"
msgstr "Th12"

#. List of long month names (may be used in "spoken" dates and date ranges).
#: ../../english/template/debian/ctime.wml:39
msgid "January"
msgstr "Tháng Giêng"

#: ../../english/template/debian/ctime.wml:40
msgid "February"
msgstr "Tháng Hai"

#: ../../english/template/debian/ctime.wml:41
msgid "March"
msgstr "Tháng Ba"

#: ../../english/template/debian/ctime.wml:42
msgid "April"
msgstr "Tháng Tư"

#. The <void> tag is to distinguish short and long forms of May.
#. Do not put it in msgstr.
#: ../../english/template/debian/ctime.wml:45
msgid "<void id=\"fullname\" />May"
msgstr "Tháng Năm"

#: ../../english/template/debian/ctime.wml:46
msgid "June"
msgstr "Tháng Sáu"

#: ../../english/template/debian/ctime.wml:47
msgid "July"
msgstr "Tháng Bảy"

#: ../../english/template/debian/ctime.wml:48
msgid "August"
msgstr "Tháng Tám"

#: ../../english/template/debian/ctime.wml:49
msgid "September"
msgstr "Tháng Chín"

#: ../../english/template/debian/ctime.wml:50
msgid "October"
msgstr "Tháng Mười"

#: ../../english/template/debian/ctime.wml:51
msgid "November"
msgstr "Tháng Mười Một"

#: ../../english/template/debian/ctime.wml:52
msgid "December"
msgstr "Tháng Mười Hai"

#. $dateform: Date format (sprintf) for modification dates.
#. Available variables are: $mday = day-of-month, $monnr = month number,
#. $mon = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:60
msgid ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"
msgstr ""
"q{[%]02d:[%]02d:[%]02d [%]s [%]s, [%]2d [%]s [%]04d}, $hour, $min, $sec, "
"q{UTC}, $wday, $mday, $mon, 1900+$year"

#. $newsdateform: Date format (sprintf) for news items.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:68
msgid "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"
msgstr "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"

#. $spokendateform: Date format (sprintf) for "spoken" dates
#. (such as the current release date).
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:79
msgid "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"
msgstr "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"

#. $spokendateform_noyear: Date format (sprintf) for "spoken" dates
#. (such as the current release date), without the year.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy).
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:90
msgid "q{[%]d [%]s}, $mday, $mon_str"
msgstr "q{[%]d [%]s}, $mday, $mon_str"

#. $spokendateform_noday: Date format (sprintf) for "spoken" dates
#. (such a conference event), without the day.
#. Available variables are: $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:99
msgid "q{[%]s [%]s}, $mon_str, $year"
msgstr "q{[%]s [%]s}, $mon_str, $year"

#. $rangeform_samemonth: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges within the same month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday = end
#. day-of-month, $smon = month number, $smon_str = month string (from @longmoy)
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:110
msgid "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"
msgstr "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"

#. $rangeform_severalmonths: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges spanning the end of a month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday, end
#. day-of-month, $smon = start month number, $emon = end month number,
#. $smon_str = start month string (from @longmoy), $emon_str = end month string
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:122
msgid "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
msgstr "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
